const cvs = document.getElementById("snake");
const ctx = cvs.getContext("2d");
//create player
let userName =  prompt("enter your name");
//create the unit
const box = 32;
let cW = cvs.width;//get canvas width
let cH = cvs.height;//get canvas height
//load images
//
const ground = new Image();
ground.src = "ground.png";///groundIMage

const foodImg = new Image();
foodImg.src= "food.png";//appleImage
//create snake array
let snake = [];
snake[0] = {
  x: 9 * box,
  y: 10 * box
}
let startGame = false;//check if game started;
let gameOver = false;//check if game end
// Check browser support for localStorage
if (typeof(Storage) !== "undefined") {
    // Store snakeLVL
    if(localStorage.getItem("snakeLVL") === null){
      let snakeLVL = prompt("Enter snake level");
      localStorage.setItem("snakeLVL", snakeLVL);
    }
      // Retrieve snake length
      for (var i = 0; i < parseInt(localStorage.getItem("snakeLVL")); i++) {
        snake.push(snake[0])
      }

} else {
    document.getElementById("snake").innerHTML = "Sorry, your browser does not support Web Storage...";
}


//create food position
let food = {
  x: Math.floor(Math.random()*17+1)*box,
  y: Math.floor(Math.random()*15+3)*box
           }

let score = 0 ;//create score

// cheack collision function
function collision(head,array){
    for(let i = 0; i < array.length; i++){
        if(head.x == array[i].x && head.y == array[i].y && snake[0].x != 9 * box && snake[0].y != 10 * box ){
            return true;
        }
    }
    return false;
}
//draw canvas

      function draw(){
        ctx.drawImage(ground,0,0);

        for(let i = 0; i < snake.length; i++){
          ctx.fillStyle = (i==0) ? "green":"blue";//make tail blue and head green
          ctx.fillRect(snake[i].x,snake[i].y,box,box);

          ctx.strokeStyle = "red";//make border red
          ctx.strokeRect(snake[i].x,snake[i].y,box,box);
        }
        ctx.drawImage(foodImg,food.x,food.y);//draw food in canvas
        //snake head position(old)
        let snakeX = snake[0].x;
        let snakeY = snake[0].y;
        //choose directtion
        if(d == "LEFT") snakeX -= box;
        if(d == "UP") snakeY -= box;
        if(d == "RIGHT") snakeX += box;
        if(d == "DOWN") snakeY += box;

        // fatsnake (check if snake ate food)
        if(snakeX == food.x && snakeY == food.y){
          score++;
           food = {
            x: Math.floor(Math.random()*17+1)*box,
            y: Math.floor(Math.random()*15+3)*box
                     }

        }else{
        //remove the tail
        snake.pop();
      }

      // head(new)
      let newHead = {
        x : snakeX,
        y : snakeY
      }
      //fatality
      if(snakeX < box || snakeX >  17*box || snakeY < 3*box || snakeY > 17*box || collision(newHead,snake)){
        clearInterval(game);
        gameOver = true;
      }
      snake.unshift(newHead);

        ctx.fillStyle = "white";
        ctx.font = "45px Changa One";
        ctx.fillText(score,2*box,1.6*box);
        //check if game is over and enter data into localStorage
        if(gameOver){
          storeUser(userName);
          showGameOver();
        }
      }

      let game = setInterval(draw,100);//call draw(refresh frames) every 100 ms
      document.addEventListener("keydown",direction);
      //check if game started
          let fired = false;
          direction.onkeydown = function() {
        if(!fired) {
            fired = true;
            if(startGame === false){
              startGame = true;
            }
        }
      };
  // end check if game started
      //controll snake
      let d;
      function direction(event){
        if(event.keyCode == 37 && d != "RIGHT"){
            d = "LEFT";
        }else if(event.keyCode == 38 && d != "DOWN"){
            d = "UP";
        }else if(event.keyCode == 39 && d != "LEFT"){
            d = "RIGHT";
        }else if(event.keyCode == 40 && d != "UP"){
            d = "DOWN";
        }

        if(gameOver === true){//check if game is over
        //if user click R refresh Game
        if(event.keyCode == 82){
          clearInterval(game);
          snake[0] = {
            x: 9 * box,
            y: 10 * box
          }
          food = {
            x: Math.floor(Math.random()*17+1)*box,
            y: Math.floor(Math.random()*15+3)*box
                }
          score = 0;
          game = setInterval(draw,100)//renew game
          // startGame = false;
          fired = false;
          gameOver = false;

          if(snake.length-1 > parseInt(localStorage.getItem("snakeLVL"))){
            for (var i = snake.length-1; i > parseInt(localStorage.getItem("snakeLVL")); i--) {
                snake.pop();
              }
          }
        }
        //if user click T reload page to enter new user
        if(event.keyCode == 84){
          location.reload();
        }
      }
      }
      // Store user and score in localStorage
      function storeUser(inputName){
        if (typeof(Storage) !== "undefined") {
            //store player
            if(JSON.parse(localStorage.getItem("users")) === null){
              let users = [{}];
              users[0].username = userName;
              users[0].score = score;
              localStorage.setItem("users", JSON.stringify(users));
            }else{
              let users = JSON.parse(localStorage.getItem("users"));

            for (let i = 0; i < users.length; i++) {
              if(inputName == users[i].username){  //look for match with name
                if(users[i].score < score){
                users[i].score = score;
                localStorage.setItem("users", JSON.stringify(users));
                }
                break;  //exit loop since you found the person
              }else{
                if(i == users.length-1){
           let stored = JSON.parse(localStorage.getItem("users"));
           user = { "username": inputName , "score":score };
           stored.push(user);
           localStorage.setItem("users", JSON.stringify(stored));
         }
          }
        }
      }
        }
      }
      //show Game Over
      function showGameOver() {
        ctx.clearRect(0, 0, cW, cH);
        ctx.beginPath();
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, cW, cH);
        ctx.closePath();

        ctx.beginPath();
        ctx.fillStyle = '#fff';
        ctx.font = '20pt Arial';
        ctx.fillText('Game Over', 230, cH / 2);
        ctx.closePath();

        ctx.beginPath();
        ctx.fillStyle = '#fff';
        ctx.font = '20pt Arial';
        ctx.fillText('Click R to restart', 200 , 400 );
        ctx.closePath();

        ctx.beginPath();
        ctx.fillStyle = '#fff';
        ctx.font = '20pt Arial';
        ctx.fillText('Click T to enter new user', 150 , 450 );
        ctx.closePath();


        ctx.beginPath();
        ctx.fillStyle = 'red';
        ctx.font = '15pt Arial';
          let users = JSON.parse(localStorage.getItem("users"));
          for (let i = 0; i < users.length; i++) {
            if(userName == users[i].username){  //look for match with name
            ctx.fillText(`Your max score achieved is: ${users[i].score}`, 10, 20)
              }
            }
          ctx.fillText(`your current score is: ${score}`, 10, 50);
          ctx.closePath();

      }
